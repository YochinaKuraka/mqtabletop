# MQTabletop

A protocol for playing card games over MQTT, also contains a minimal windows client implementation.

No rights to any images used are owned by me, the deck supplied with the software is omnia suprema by thirdway industries.

imagesources: 
* https://www.thirdwayindustries.com/product/omnia-suprema-uncut-sheet/
* https://www.kickstarter.com/projects/thirdwayind/omnia-playing-cards
