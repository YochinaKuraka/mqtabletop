﻿using MQTabletop.Core.Contracts;
using MQTabletop.Core.Contracts.Messages;
using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTabletop.Core
{
    public class MqTabletopClient
        : IDisposable
    {
        private IManagedMqttClient _mqttClient;
        private string _topic;
        private MessageDeserializer _messageDeserializer;
        private GameStateManager _gameStateManager;

        public Func<string, IEnumerable<string>, Task> NewPlayerHandler 
        {
            get => _gameStateManager.NewPlayerHandler;
            set => _gameStateManager.NewPlayerHandler = value; 
        }

        public Func<string, Task> GameStartedHandler
        {
            get => _gameStateManager.GameStartedHandler;
            set => _gameStateManager.GameStartedHandler = value;
        }

        public Func<GameState, IMessage, Task> GameStateUpdatedHandler
        {
            get => _gameStateManager.GameStateUpdatedHandler;
            set => _gameStateManager.GameStateUpdatedHandler = value;
        }

        public GameState GameState => _gameStateManager.GameState;
        public object GameStateLock => _gameStateManager.GameStateLock;
        public string IngameName => _gameStateManager.InGameName;

        public MqTabletopClient()
        {
            _messageDeserializer = new();
            _gameStateManager = new();
        }

        public async Task ConnectAsync(string broker, string user, string password, 
            string topic, string clientId, bool useTls, string inGameName)
        {
            if (_mqttClient != null)
            {
                throw new InvalidOperationException("Already connected");
            }

            if (!broker.Contains("://"))
            {
                if (!broker.Contains(":"))
                {
                    broker = $"{broker}:1883";
                }

                broker = $"http://{broker}";
            }

            if (!Uri.TryCreate(broker, UriKind.Absolute, out var brokerUri))
            {
                throw new ArgumentException("Invalid server address");
            }

            var clientOptionsBuilder = new MqttClientOptionsBuilder()
                   .WithClientId(clientId);

            if (!(string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password)))
            {
                clientOptionsBuilder.WithCredentials(user, Encoding.UTF8.GetBytes(password));
            }

            if (brokerUri.Scheme == "http")
            {
                clientOptionsBuilder.WithTcpServer(brokerUri.DnsSafeHost, brokerUri.Port);

                if (useTls)
                {
                    clientOptionsBuilder.WithTls();
                }
            }
            else
            {
                clientOptionsBuilder.WithWebSocketServer(broker);
            }

            var optionsBuilder = new ManagedMqttClientOptionsBuilder()
               .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
               .WithClientOptions(clientOptionsBuilder);

            _mqttClient = new MqttFactory().CreateManagedMqttClient();

            _mqttClient.UseConnectedHandler(OnMqttClientConnectedAsync);
            _mqttClient.UseDisconnectedHandler(OnMqttClientDisconnectedAsync);
            _mqttClient.UseApplicationMessageReceivedHandler(OnMqttApplicationMessageReceived);

            await _mqttClient.StartAsync(optionsBuilder.Build());
            await _mqttClient.SubscribeAsync(topic, MqttQualityOfServiceLevel.ExactlyOnce);
            _gameStateManager.InGameName = inGameName;
            _topic = topic;

            _gameStateManager.ReSendHelloHandler = ReSendHello;
        }

        private async Task ReSendHello()
        {
            await SendAsync(new HelloMessage()
            {
                InGameName = _gameStateManager.InGameName,
            });
        }

        public async Task SendMessage(IMessage message)
        {
            ValidateCanBeSentByUser(message);
            await SendAsync(message);
        }

        private static void ValidateCanBeSentByUser(IMessage message)
        {
            if (!(message is PutCardMessage || message is TakeCardMessage))
            {
                throw new ArgumentException("invalid message type");
            }
        }

        public async Task SendMessages(IEnumerable<IMessage> messages)
        {
            foreach (var message in messages)
            {
                ValidateCanBeSentByUser(message);
            }

            var messagesText = string.Join(Environment.NewLine, messages.Select(m => GetMessageText(m)));
            await _mqttClient.PublishAsync(_topic, messagesText, MqttQualityOfServiceLevel.ExactlyOnce);
        }

        private async Task OnMqttApplicationMessageReceived(MqttApplicationMessageReceivedEventArgs arg)
        {
            var mqTableTopMessages = Encoding.UTF8.GetString(arg.ApplicationMessage.Payload)
                .Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            foreach (var mqTableTopMessage in mqTableTopMessages)
            {
                var message = _messageDeserializer.Deserialize(mqTableTopMessage);

                if (message is HelloMessage helloMessage)
                {
                    await _gameStateManager.HandleHelloMessageAsync(helloMessage);
                }
                else if (message is StartMessage startMessage)
                {
                    await _gameStateManager.HandleStartMessageAsync(startMessage);
                }
                else if (message is PutCardMessage putCardMessage)
                {
                    await _gameStateManager.HandlePutCardMessageAsync(putCardMessage);
                }
                else if (message is TakeCardMessage takeCardMessage)
                {
                    await _gameStateManager.HandleTakeCardMessageAsync(takeCardMessage);
                }
            }
        }

        private async Task SendAsync(IMessage message)
        {
            await _mqttClient.PublishAsync(_topic, GetMessageText(message), MqttQualityOfServiceLevel.ExactlyOnce);
        }

        private string GetMessageText(IMessage message)
        {
            return $"{_gameStateManager.InGameName}:{message.Serialize()}";
        }

        private Task OnMqttClientDisconnectedAsync(MqttClientDisconnectedEventArgs arg)
        {
            return Task.CompletedTask;
        }

        private async Task OnMqttClientConnectedAsync(MqttClientConnectedEventArgs arg)
        {
            await SendAsync(new HelloMessage()
            {
                InGameName = _gameStateManager.InGameName,
            });
        }

        public void Dispose()
        {
            if (_mqttClient != null)
            {
                _mqttClient.Dispose();
            }
        }

        public async Task StartGame()
        {
            await SendAsync(new StartMessage());
        }
    }
}
