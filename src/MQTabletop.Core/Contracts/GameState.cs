﻿using System.Collections.Generic;

namespace MQTabletop.Core.Contracts
{
    public class GameState
    {
        public List<Card> Cards { get; set; } = new();
        public List<CardStack> Stacks { get; set; } = new();
        public Dictionary<string, List<Card>> PlayersCards { get; set; } = new();
    }
}
