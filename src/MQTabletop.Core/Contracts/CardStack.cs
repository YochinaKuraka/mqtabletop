﻿using System.Collections.Generic;

namespace MQTabletop.Core.Contracts
{
    public class CardStack
    {
        public List<Card> Cards { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}