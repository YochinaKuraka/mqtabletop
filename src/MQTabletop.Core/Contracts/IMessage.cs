﻿namespace MQTabletop.Core.Contracts
{
    public interface IMessage
    {
        string Author { get; set; }

        string Serialize();
    }
}
