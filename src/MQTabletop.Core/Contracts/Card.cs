﻿namespace MQTabletop.Core.Contracts
{
    public class Card
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsUpsideDown { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}