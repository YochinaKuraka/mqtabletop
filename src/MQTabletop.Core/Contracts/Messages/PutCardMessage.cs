﻿using System;

namespace MQTabletop.Core.Contracts.Messages
{
    public class PutCardMessage : IMessage
    {
        public string Author { get; set; }
        public string Card { get; set; }
        public string Id { get; set; }
        public string PutOntoCardId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Flip { get; set; }
        public bool MoveCompleteStack { get; set; }

        public string Serialize()
        {
            var result = $"put {Card} {Id}";
            
            if (!string.IsNullOrEmpty(PutOntoCardId))
            {
                result += $" onto {PutOntoCardId}";
            }
            else
            {
                result += $" {X} {Y}";
            }

            if (Flip)
            {
                result += " flip";
            }

            if (MoveCompleteStack)
            {
                result += " stack";
            }

            return result;
        }
    }
}
