﻿namespace MQTabletop.Core.Contracts.Messages
{
    public class StartMessage : IMessage
    {
        public string Author { get; set; }

        public string Serialize()
        {
            return "start";
        }
    }
}
