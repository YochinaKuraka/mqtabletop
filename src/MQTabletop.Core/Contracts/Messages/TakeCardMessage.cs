﻿namespace MQTabletop.Core.Contracts.Messages
{
    public class TakeCardMessage : IMessage
    {
        public string Author { get; set; }
        public string TakeCardId { get; set; }
        public bool TakeStack { get; set; }

        public string Serialize()
        {
            var result = $"take {TakeCardId}";

            if (TakeStack)
            {
                result += " stack";
            }

            return result;
        }
    }
}
