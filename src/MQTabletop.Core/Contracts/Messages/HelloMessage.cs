﻿namespace MQTabletop.Core.Contracts.Messages
{
    public class HelloMessage : IMessage
    {
        public string Author { get; set; }
        public string InGameName { get; set; }

        public string Serialize()
        {
            return $"hello {InGameName}";
        }
    }
}
