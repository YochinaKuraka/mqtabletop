﻿using MQTabletop.Core.Contracts;
using MQTabletop.Core.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MQTabletop.Core
{
    public class GameStateManager
    {
        public bool GameStarted { get; private set; }
        public string InGameName { get; set; }
        public GameState GameState { get; set; } = new();
        public object GameStateLock { get; private set; } = new();

        public Func<string, IEnumerable<string>, Task> NewPlayerHandler { get; set; }
        public Func<Task> ReSendHelloHandler { get; set; }
        public Func<string, Task> GameStartedHandler { get; set; }
        public Func<GameState, IMessage, Task> GameStateUpdatedHandler { get; set; }

        internal Task HandleHelloMessageAsync(HelloMessage helloMessage)
        {
            if (GameStarted)
            {
                return Task.CompletedTask;
            }

            if (GameState.PlayersCards.ContainsKey(helloMessage.InGameName))
            {
                return Task.CompletedTask;
            }

            if (ReSendHelloHandler != null)
            {
                ReSendHelloHandler();
            }

            lock (GameStateLock)
            {
                GameState.PlayersCards.Add(helloMessage.InGameName, new());
            }

            if (NewPlayerHandler != null)
            {
                NewPlayerHandler(helloMessage.InGameName, GameState.PlayersCards.Keys.ToArray());
            }

            return Task.CompletedTask;
        }

        public Task HandleStartMessageAsync(StartMessage startMessage)
        {
            if (GameStarted)
            {
                return Task.CompletedTask;
            }

            GameStarted = true;

            if (GameStartedHandler != null)
            {
                GameStartedHandler(startMessage.Author);
            }

            return Task.CompletedTask;
        }

        public Task HandlePutCardMessageAsync(PutCardMessage putCardMessage)
        {
            if (!IsRelevant(putCardMessage))
            {
                return Task.CompletedTask;
            }

            lock (GameStateLock)
            {
                var card = UpdateCard(putCardMessage);

                if (!string.IsNullOrEmpty(putCardMessage.PutOntoCardId) && !putCardMessage.MoveCompleteStack)
                {
                    AddToStack(putCardMessage, card);
                }

                if (putCardMessage.Flip)
                {
                    card.IsUpsideDown = !card.IsUpsideDown;
                }
            }

            if (GameStateUpdatedHandler != null)
            {
                GameStateUpdatedHandler(GameState, putCardMessage);
            }

            return Task.CompletedTask;
        }

        private Card UpdateCard(PutCardMessage putCardMessage)
        {
            var card = GameState.Cards.Find(c => c.Id == putCardMessage.Id);

            if (card == null)
            {
                card = new Card()
                {
                    Id = putCardMessage.Id,
                    Name = putCardMessage.Card,
                };

                GameState.Cards.Add(card);
            }
            else
            {
                foreach (var playerCards in GameState.PlayersCards)
                {
                    playerCards.Value.Remove(card);
                }

                var stack = GameState.Stacks.Find(s => s.Cards.Contains(card));

                if (stack != null)
                {
                    if (putCardMessage.MoveCompleteStack)
                    {
                        var cardIndex = stack.Cards.IndexOf(card);
                        var newStackCards = stack.Cards.Skip(cardIndex);
                        stack.Cards = stack.Cards.Take(cardIndex).ToList();

                        if (string.IsNullOrEmpty(putCardMessage.PutOntoCardId))
                        {
                            CreateNewStack(putCardMessage, newStackCards);
                        }
                        else
                        {
                            var newStack = GameState.Stacks.Find(s => s.Cards.Any(c => c.Id == putCardMessage.PutOntoCardId));

                            if (newStack == null)
                            {
                                var putOntoCard = GameState.Cards.Find(c => c.Id == putCardMessage.PutOntoCardId);

                                if (putOntoCard != null)
                                {
                                    newStackCards = new Card[] { putOntoCard }.Union(newStackCards);
                                    putCardMessage.X = putOntoCard.X;
                                    putCardMessage.Y = putOntoCard.Y;
                                }

                                CreateNewStack(putCardMessage, newStackCards);
                            }
                            else
                            {
                                var putOntoCard = newStack.Cards.First(c => c.Id == putCardMessage.PutOntoCardId);
                                var putOntoIndex = newStack.Cards.IndexOf(putOntoCard);
                                newStack.Cards.InsertRange(putOntoIndex + 1, newStackCards);
                            }
                        }
                    }
                    else
                    {
                        stack.Cards.Remove(card);
                    }

                    if (stack.Cards.Count <= 1)
                    {
                        GameState.Stacks.Remove(stack);
                    }
                }
            }

            GameState.Cards.Remove(card);
            GameState.Cards.Add(card);
            card.X = putCardMessage.X;
            card.Y = putCardMessage.Y;
            return card;
        }

        private void CreateNewStack(PutCardMessage putCardMessage, IEnumerable<Card> newStackCards)
        {
            GameState.Stacks.Add(new()
            {
                Cards = newStackCards.ToList(),
                X = putCardMessage.X,
                Y = putCardMessage.Y,
            });

            foreach (var newStackCard in newStackCards)
            {
                newStackCard.X = putCardMessage.X;
                newStackCard.Y = putCardMessage.Y;
            }
        }

        public Task HandleTakeCardMessageAsync(TakeCardMessage takeCardMessage)
        {
            if (!IsRelevant(takeCardMessage))
            {
                return Task.CompletedTask;
            }

            lock (GameStateLock)
            {
                var cardsToTake = TakeCardsToTake(takeCardMessage);

                foreach (var cardToTake in cardsToTake)
                {
                    cardToTake.X = 0;
                    cardToTake.IsUpsideDown = false;
                }

                GameState.PlayersCards[takeCardMessage.Author].AddRange(cardsToTake);
            }

            GameStateUpdatedHandler?.Invoke(GameState, takeCardMessage);
            return Task.CompletedTask;
        }

        private IEnumerable<Card> TakeCardsToTake(TakeCardMessage takeCardMessage)
        {
            var cardToTake = GameState.Cards.Find(c => c.Id == takeCardMessage.TakeCardId);
            var result = new List<Card>();

            if (cardToTake == null)
            {
                return result;
            }

            var stack = GameState.Stacks.Find(s => s.Cards.Contains(cardToTake));

            if (stack != null)
            {
                if (takeCardMessage.TakeStack)
                {
                    var cardToTakeIndex = stack.Cards.IndexOf(cardToTake);
                    result = stack.Cards.Skip(cardToTakeIndex).ToList();
                    stack.Cards = stack.Cards.Take(cardToTakeIndex).ToList();
                }
                else
                {
                    stack.Cards.Remove(cardToTake);
                    result.Add(cardToTake);
                }

                if (stack.Cards.Count <= 1)
                {
                    GameState.Stacks.Remove(stack);
                }
            }
            else
            {
                result.Add(cardToTake);
            }

            return result;
        }

        private void AddToStack(PutCardMessage putCardMessage, Card card)
        {
            var otherCard = GameState.Cards.Find(c => c.Id == putCardMessage.PutOntoCardId);
            var stack = GameState.Stacks.Find(s => s.Cards.Contains(otherCard));

            if (otherCard == null)
            {
                return;
            }

            if (stack == null)
            {
                stack = new CardStack()
                {
                    Cards = new()
                    {
                        otherCard,
                        card,
                    },
                    X = otherCard.X,
                    Y = otherCard.Y,
                };

                GameState.Stacks.Add(stack);
            }
            else
            {
                stack.Cards.Insert(stack.Cards.IndexOf(otherCard) + 1, card);
            }

            card.X = stack.X;
            card.Y = stack.Y;
        }

        private bool IsRelevant(IMessage message)
        {
            if (!GameStarted)
            {
                return false;
            }

            if (!GameState.PlayersCards.ContainsKey(message.Author))
            {
                return false;
            }

            return true;
        }
    }
}
