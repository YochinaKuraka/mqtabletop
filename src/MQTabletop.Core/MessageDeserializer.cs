﻿using MQTabletop.Core.Contracts;
using MQTabletop.Core.Contracts.Messages;
using System;
using System.Linq;

namespace MQTabletop.Core
{
    public class MessageDeserializer
    {
        public IMessage Deserialize(string message)
        {
            try
            {
                var split = message.Split(':');
                var cleanMessage = split[0];

                if (split.Length == 2)
                {
                    cleanMessage = split[1];
                }

                var result = GetMessage(cleanMessage);

                if (result == null)
                {
                    throw new ArgumentException("invalid message string");
                }

                if (split.Length == 2)
                {
                    result.Author = split[0];
                }

                return result;
            }
            catch
            {
                return null;
            }
        }

        private IMessage GetMessage(string message)
        {
            var split = message.Split(' ');

            switch (split[0])
            {
                case "hello":
                    return ParseHello(split);
                case "start":
                    return ParseStart(split);
                case "put":
                    return ParsePut(split);
                case "take":
                    return ParseTake(split);
            }

            return null;
        }

        private IMessage ParseTake(string[] split)
        {
            return new TakeCardMessage()
            {
                TakeCardId = split[1],
                TakeStack = split.Length > 2 && split.Contains("stack"),
            };
        }

        private IMessage ParsePut(string[] split)
        {
            var flip = split.Any(a => a =="flip");
            var moveCompleteStack = split.Any(a => a == "stack");
            var putOntoCardId = string.Empty;
            var x = 0;
            var y = 0;

            if (split[3] == "onto")
            {
                putOntoCardId = split[4];
            }
            else
            {
                x = int.Parse(split[3]);
                y = int.Parse(split[4]);
            }

            return new PutCardMessage()
            {
                Card = split[1],
                Flip = flip,
                MoveCompleteStack = moveCompleteStack,
                Id = split[2],
                PutOntoCardId = putOntoCardId,
                X = x,
                Y = y,
            };
        }

        private IMessage ParseStart(string[] split)
        {
            return new StartMessage();
        }

        private IMessage ParseHello(string[] split)
        {
            return new HelloMessage()
            {
                InGameName = split[1],
            };
        }
    }
}
