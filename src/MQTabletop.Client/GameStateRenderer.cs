﻿using MQTabletop.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace MQTabletop.Client
{
    public static class GameStateRenderer
    {
        public static int CardsHigh { get; set; } = 5;
        public static int CardRows { get; set; } = 100;
        public static int CardColumns { get; set; } = 150;

        public static Image RenderGameState(GameState gameState, int width, int height, object gameStateLock)
        {
            lock (gameStateLock)
            {
                if (width <= 0 || height <= 0)
                {
                    throw new ArgumentException("invalid size");
                }

                var image = new Bitmap(width, height);
                using var graphics = Graphics.FromImage(image);
                var cardSize = GetCardSize(width, height);

                foreach (var stack in gameState.Stacks)
                {
                    var stackLocation = GetCardLocation(stack.X, stack.Y, width, height, cardSize);
                    var stackImage = GetImage("stack");
                    var stackImageOffset = GetStackImageOffset(cardSize, stack.Cards.Count);
                    graphics.DrawImage(stackImage, new Rectangle(stackLocation, cardSize));
                    stackLocation.Y -= stackImageOffset;
                    var lastCard = stack.Cards.Last();
                    DrawCardAtLocation(graphics, cardSize, lastCard, stackLocation);
                }

                var boardCards = gameState.Cards.Where(c => !gameState.Stacks.Any(s => s.Cards.Contains(c))
                                                            && !gameState.PlayersCards.Any(p => p.Value.Contains(c))).ToList();

                foreach (var card in boardCards)
                {
                    DrawCard(width, height, graphics, cardSize, card);
                }
                
                return image;
            }
        }

        private static int GetStackImageOffset(Size cardSize, int count)
        {
            var fullStackSize = 54;
            var fullStackOffset = cardSize.Height / 3;
            return fullStackOffset * count / fullStackSize;
        }

        private static void DrawCard(int width, int height, Graphics graphics, Size cardSize, Card card)
        {
            var cardLocation = GetCardLocation(card.X, card.Y, width, height, cardSize);
            DrawCardAtLocation(graphics, cardSize, card, cardLocation);
        }

        private static void DrawCardAtLocation(Graphics graphics, Size cardSize, Card card, Point cardLocation)
        {
            var cardImage = GetCardImage(card);
            graphics.DrawImage(cardImage, new Rectangle(cardLocation, cardSize));
        }

        private static Point GetCardLocation(int x, int y, int tableWidth, int tableHeight, Size cardSize)
        {
            var positionRangeX = tableWidth - cardSize.Width;
            var positionRangeY = tableHeight - cardSize.Height;
            return new Point(positionRangeX / CardColumns * x, positionRangeY / CardRows * y);
        }

        public static Image RenderHandCards(GameState gameState, int height, int width, string ingameName, object gameStateLock)
        {
            lock (gameStateLock)
            {
                var handCards = gameState.PlayersCards[ingameName];
                var cardHeight = height * 50 / 55;
                var cardWidth = GetCardWidth(cardHeight);
                var image = new Bitmap(width, height);
                using var graphics = Graphics.FromImage(image);

                foreach (var handCard in handCards.OrderBy(h => h.X))
                {
                    graphics.DrawImage(GetCardImage(handCard), new Rectangle(handCard.X, (height - cardHeight) / 2, cardWidth, cardHeight));
                }

                return image;
            }
        }

        private static Size GetCardSize(int width, int height)
        {
            var cardHeight = height / CardsHigh;
            int cardWidth = GetCardWidth(cardHeight);
            return new Size(cardWidth, cardHeight);
        }

        private static int GetCardWidth(int cardHeight)
        {
            return 153 * cardHeight / 209;
        }

        private static Image GetCardImage(Card card)
        {
            var imageName = card.Name;

            if (card.IsUpsideDown)
            {
                imageName = "back";
            }

            return GetImage(imageName);
        }

        private static Image GetImage(string imageName)
        {
            var path = Path.Combine("cards", $"{imageName}.png");

            if (!File.Exists(path))
            {
                return new Bitmap(10, 10);
            }

            return Image.FromFile(path);
        }

        public static Card GetCardAtPosition(GameState gameState, Point relativeClickPosition, int width, int height, IEnumerable<Card> exceptFor, out CardStack stack, bool getLowest = false)
        {
            var card = GetCardAtPosition(gameState, relativeClickPosition, width, height, exceptFor, getLowest);
            stack = gameState.Stacks.Find(s => s.Cards.Contains(card));
            return card;
        }

        public static Card GetCardAtPosition(GameState gameState, Point relativeClickPosition, int width, int height, IEnumerable<Card> exceptFor, bool getLowest = false)
        {
            if (exceptFor == null)
            {
                exceptFor = Enumerable.Empty<Card>();
            }

            var unstackedCards = gameState.Cards
                .Where(c => !gameState.Stacks
                    .Any(s => s.Cards.Contains(c)));

            var cardsUnderPointer = unstackedCards.Where(c => IsCardUnderPointer(c, relativeClickPosition, width, height));
            var cardsUnderPointerExceptFor = cardsUnderPointer.Where(c => !exceptFor.Contains(c));

            if (cardsUnderPointerExceptFor.Any())
            {
                if (getLowest)
                {
                    return cardsUnderPointerExceptFor.First();
                }

                return cardsUnderPointerExceptFor.Last();
            }

            var stacksUnderPointer = gameState.Stacks.Where(s => IsStackUnderPointer(s, relativeClickPosition, width, height));
            var stacksUnderPointerExceptFor = stacksUnderPointer.Where(s => s.Cards.Any(c => !exceptFor.Contains(c)));

            if (stacksUnderPointerExceptFor.Any())
            {
                if (getLowest)
                {
                    return stacksUnderPointerExceptFor
                    .First().Cards.First(c => !exceptFor.Contains(c));
                }

                return stacksUnderPointerExceptFor
                    .Last().Cards.Last(c => !exceptFor.Contains(c));
            }

            return null;
        }

        private static bool IsStackUnderPointer(CardStack stack, Point relativeClickPosition, int width, int height)
        {
            var stackSize = GetCardSize(width, height);
            var stackLocation = GetCardLocation(stack.X, stack.Y, width, height, stackSize);
            var stackImageOffset = GetStackImageOffset(stackSize, stack.Cards.Count);
            stackLocation.Y -= stackImageOffset;
            stackSize.Height += stackImageOffset;
            return (new Rectangle(stackLocation, stackSize)).Contains(relativeClickPosition);
        }

        private static bool IsCardUnderPointer(Card card, Point relativeClickPosition, int width, int height)
        {
            var cardSize = GetCardSize(width, height);
            var cardLocation = GetCardLocation(card.X, card.Y, width, height, cardSize);
            return (new Rectangle(cardLocation, cardSize)).Contains(relativeClickPosition);
        }

        internal static Point GetPositionFromLocation(int width, int height, int x, int y)
        {
            var cardSize = GetCardSize(width, height);
            var positionRangeX = width - cardSize.Width;
            var positionRangeY = height - cardSize.Height;
            return new Point(x / (positionRangeX / CardColumns), y / (positionRangeY / CardRows));
        }

        internal static Card GetHandCardAtPosition(GameState gameState, string ingameName, int height, Point location)
        {
            var handCards = gameState.PlayersCards[ingameName];
            var cardHeight = height * 50 / 55;
            var cardWidth = GetCardWidth(cardHeight);
            var cardsUnderPointer = handCards.Where(c => c.X <= location.X && c.X + cardWidth >= location.X);

            if (!cardsUnderPointer.Any())
            {
                return null;
            }

            return cardsUnderPointer.OrderBy(c => c.X).Last();
        }
    }
}
