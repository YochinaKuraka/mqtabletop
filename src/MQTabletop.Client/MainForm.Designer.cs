﻿
using System;

namespace MQTabletop.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this._textBoxDebug = new System.Windows.Forms.TextBox();
            this._pictureBoxMain = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addShuffledDeckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addShuffledDeck52CardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._buttonStartGame = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._textBoxPlayers = new System.Windows.Forms.TextBox();
            this._pictureBoxHandCards = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxMain)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxHandCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _textBoxDebug
            // 
            this._textBoxDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxDebug.Enabled = false;
            this._textBoxDebug.Location = new System.Drawing.Point(576, 218);
            this._textBoxDebug.Multiline = true;
            this._textBoxDebug.Name = "_textBoxDebug";
            this._textBoxDebug.Size = new System.Drawing.Size(224, 191);
            this._textBoxDebug.TabIndex = 0;
            // 
            // _pictureBoxMain
            // 
            this._pictureBoxMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._pictureBoxMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pictureBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pictureBoxMain.Location = new System.Drawing.Point(0, 0);
            this._pictureBoxMain.Name = "_pictureBoxMain";
            this._pictureBoxMain.Size = new System.Drawing.Size(570, 213);
            this._pictureBoxMain.TabIndex = 1;
            this._pictureBoxMain.TabStop = false;
            this._pictureBoxMain.SizeChanged += new System.EventHandler(this._pictureBoxMain_SizeChanged);
            this._pictureBoxMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this._pictureBoxMain_MouseDown);
            this._pictureBoxMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this._pictureBoxMain_MouseMove);
            this._pictureBoxMain.MouseUp += new System.Windows.Forms.MouseEventHandler(this._pictureBoxMain_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addShuffledDeckToolStripMenuItem,
            this.addShuffledDeck52CardsToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // addShuffledDeckToolStripMenuItem
            // 
            this.addShuffledDeckToolStripMenuItem.Name = "addShuffledDeckToolStripMenuItem";
            this.addShuffledDeckToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.addShuffledDeckToolStripMenuItem.Text = "Add Shuffled Deck (54 cards)";
            this.addShuffledDeckToolStripMenuItem.Click += new System.EventHandler(this.AddShuffledDeckToolStripMenuItem_Click);
            // 
            // addShuffledDeck52CardsToolStripMenuItem
            // 
            this.addShuffledDeck52CardsToolStripMenuItem.Name = "addShuffledDeck52CardsToolStripMenuItem";
            this.addShuffledDeck52CardsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.addShuffledDeck52CardsToolStripMenuItem.Text = "Add Shuffled Deck (52 cards)";
            this.addShuffledDeck52CardsToolStripMenuItem.Click += new System.EventHandler(this.addShuffledDeck52CardsToolStripMenuItem_Click);
            // 
            // _buttonStartGame
            // 
            this._buttonStartGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._buttonStartGame.Location = new System.Drawing.Point(713, 415);
            this._buttonStartGame.Name = "_buttonStartGame";
            this._buttonStartGame.Size = new System.Drawing.Size(75, 23);
            this._buttonStartGame.TabIndex = 3;
            this._buttonStartGame.Text = "Start Game";
            this._buttonStartGame.UseVisualStyleBackColor = true;
            this._buttonStartGame.Click += new System.EventHandler(this._buttonStartGame_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(576, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Players";
            // 
            // _textBoxPlayers
            // 
            this._textBoxPlayers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxPlayers.Enabled = false;
            this._textBoxPlayers.Location = new System.Drawing.Point(576, 43);
            this._textBoxPlayers.Multiline = true;
            this._textBoxPlayers.Name = "_textBoxPlayers";
            this._textBoxPlayers.Size = new System.Drawing.Size(224, 169);
            this._textBoxPlayers.TabIndex = 5;
            // 
            // _pictureBoxHandCards
            // 
            this._pictureBoxHandCards.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._pictureBoxHandCards.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pictureBoxHandCards.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pictureBoxHandCards.Location = new System.Drawing.Point(0, 0);
            this._pictureBoxHandCards.Name = "_pictureBoxHandCards";
            this._pictureBoxHandCards.Size = new System.Drawing.Size(570, 209);
            this._pictureBoxHandCards.TabIndex = 6;
            this._pictureBoxHandCards.TabStop = false;
            this._pictureBoxHandCards.MouseDown += new System.Windows.Forms.MouseEventHandler(this._pictureBoxHandCards_MouseDown);
            this._pictureBoxHandCards.MouseMove += new System.Windows.Forms.MouseEventHandler(this._pictureBoxHandCards_MouseMove);
            this._pictureBoxHandCards.MouseUp += new System.Windows.Forms.MouseEventHandler(this._pictureBoxHandCards_MouseUp);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._pictureBoxMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this._pictureBoxHandCards);
            this.splitContainer1.Size = new System.Drawing.Size(570, 426);
            this.splitContainer1.SplitterDistance = 213;
            this.splitContainer1.TabIndex = 7;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this._buttonStartGame);
            this.Controls.Add(this._textBoxDebug);
            this.Controls.Add(this._textBoxPlayers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MQTabletop";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxMain)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxHandCards)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _textBoxDebug;
        private System.Windows.Forms.PictureBox _pictureBoxMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addShuffledDeckToolStripMenuItem;
        private System.Windows.Forms.Button _buttonStartGame;
        private System.Windows.Forms.ToolStripMenuItem addShuffledDeck52CardsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _textBoxPlayers;
        private System.Windows.Forms.PictureBox _pictureBoxHandCards;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

