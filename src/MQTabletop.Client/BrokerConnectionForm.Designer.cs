﻿
namespace MQTabletop.Client
{
    partial class BrokerConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._checkBoxUseTls = new System.Windows.Forms.CheckBox();
            this._textBoxServer = new System.Windows.Forms.TextBox();
            this._textBoxUsername = new System.Windows.Forms.TextBox();
            this._textBoxPassword = new System.Windows.Forms.TextBox();
            this._textBoxClientId = new System.Windows.Forms.TextBox();
            this._textBoxTopic = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._textBoxIngameName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Client Id";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Topic";
            // 
            // _checkBoxUseTls
            // 
            this._checkBoxUseTls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._checkBoxUseTls.AutoSize = true;
            this._checkBoxUseTls.Location = new System.Drawing.Point(222, 21);
            this._checkBoxUseTls.Name = "_checkBoxUseTls";
            this._checkBoxUseTls.Size = new System.Drawing.Size(66, 17);
            this._checkBoxUseTls.TabIndex = 5;
            this._checkBoxUseTls.Text = "use TLS";
            this._checkBoxUseTls.UseVisualStyleBackColor = true;
            // 
            // _textBoxServer
            // 
            this._textBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxServer.Location = new System.Drawing.Point(93, 19);
            this._textBoxServer.Name = "_textBoxServer";
            this._textBoxServer.Size = new System.Drawing.Size(123, 20);
            this._textBoxServer.TabIndex = 6;
            // 
            // _textBoxUsername
            // 
            this._textBoxUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxUsername.Location = new System.Drawing.Point(93, 45);
            this._textBoxUsername.Name = "_textBoxUsername";
            this._textBoxUsername.Size = new System.Drawing.Size(195, 20);
            this._textBoxUsername.TabIndex = 7;
            // 
            // _textBoxPassword
            // 
            this._textBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxPassword.Location = new System.Drawing.Point(93, 71);
            this._textBoxPassword.Name = "_textBoxPassword";
            this._textBoxPassword.PasswordChar = '*';
            this._textBoxPassword.Size = new System.Drawing.Size(195, 20);
            this._textBoxPassword.TabIndex = 8;
            // 
            // _textBoxClientId
            // 
            this._textBoxClientId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxClientId.Location = new System.Drawing.Point(93, 97);
            this._textBoxClientId.Name = "_textBoxClientId";
            this._textBoxClientId.Size = new System.Drawing.Size(195, 20);
            this._textBoxClientId.TabIndex = 9;
            // 
            // _textBoxTopic
            // 
            this._textBoxTopic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxTopic.Location = new System.Drawing.Point(93, 120);
            this._textBoxTopic.Name = "_textBoxTopic";
            this._textBoxTopic.Size = new System.Drawing.Size(195, 20);
            this._textBoxTopic.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(213, 193);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Tag = "WW";
            this.button1.Text = "Join";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Join_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Ingame Name";
            // 
            // _textBoxIngameName
            // 
            this._textBoxIngameName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxIngameName.Location = new System.Drawing.Point(93, 143);
            this._textBoxIngameName.Name = "_textBoxIngameName";
            this._textBoxIngameName.Size = new System.Drawing.Size(195, 20);
            this._textBoxIngameName.TabIndex = 13;
            // 
            // BrokerConnectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 228);
            this.Controls.Add(this._textBoxIngameName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._textBoxTopic);
            this.Controls.Add(this._textBoxClientId);
            this.Controls.Add(this._textBoxPassword);
            this.Controls.Add(this._textBoxUsername);
            this.Controls.Add(this._textBoxServer);
            this.Controls.Add(this._checkBoxUseTls);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BrokerConnectionForm";
            this.Text = "BrokerConectionForm";
            this.Load += new System.EventHandler(this.BrokerConnectionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox _checkBoxUseTls;
        private System.Windows.Forms.TextBox _textBoxServer;
        private System.Windows.Forms.TextBox _textBoxUsername;
        private System.Windows.Forms.TextBox _textBoxPassword;
        private System.Windows.Forms.TextBox _textBoxClientId;
        private System.Windows.Forms.TextBox _textBoxTopic;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _textBoxIngameName;
    }
}