﻿using MQTabletop.Core;
using MQTabletop.Core.Contracts;
using MQTabletop.Core.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQTabletop.Client
{
    public partial class MainForm : Form
    {
        private MqTabletopClient _mqTabletopClient;
        private Card _movingCard;
        private IEnumerable<Card> _movingStack;
        private Point _pointerStartLocation;
        private int _movingCardStartX;
        private int _movingCardStartY;
        private DateTime _lastSentMouseMove;
        private bool _moveCompleteStack;
        private Card _movingHandCard;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            var connectionForm = new BrokerConnectionForm();

            if (connectionForm.ShowDialog() != DialogResult.OK)
            {
                Close();
                return;
            }

            if (string.IsNullOrEmpty(connectionForm.Broker) ||
                string.IsNullOrEmpty(connectionForm.Topic))
            {
                MessageBox.Show("invalid broker or topic");
                Close();
                return;
            }

            _mqTabletopClient = new MqTabletopClient();
            _mqTabletopClient.NewPlayerHandler = NewPlayerHandler;
            _mqTabletopClient.GameStartedHandler = GameStartedHandler;
            _mqTabletopClient.GameStateUpdatedHandler = GameStateUpdatedHandler;

            _mqTabletopClient.ConnectAsync(connectionForm.Broker,
                connectionForm.User,
                connectionForm.Password,
                connectionForm.Topic,
                connectionForm.ClientId,
                connectionForm.UseTls,
                connectionForm.IngameUser).Wait();
        }

        private Task GameStateUpdatedHandler(GameState arg1, IMessage arg2)
        {
            Render(arg1);
            return Task.CompletedTask;
        }

        private void Render(GameState gameState)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                return;
            }

            Image mainImage;

            try
            {
                mainImage = GameStateRenderer.RenderGameState(gameState, _pictureBoxMain.Width, _pictureBoxMain.Height,
                    _mqTabletopClient.GameStateLock);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Render Error: {ex}");
                throw;
            }

            Image handCardsImage;

            try
            {
                handCardsImage = GameStateRenderer.RenderHandCards(gameState, _pictureBoxHandCards.Height, _pictureBoxHandCards.Width,
                    _mqTabletopClient.IngameName, _mqTabletopClient.GameStateLock);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Render Error: {ex}");
                throw;
            }

            var playersString = string.Join(Environment.NewLine, gameState.PlayersCards.Select(kvp => $"{kvp.Key} ({kvp.Value.Count})"));

            Invoke(new Action(() =>
            {
                _pictureBoxMain.Image = mainImage;
                _pictureBoxHandCards.Image = handCardsImage;
                _textBoxPlayers.Text = playersString;
            }));
        }

        private Task GameStartedHandler(string startedBy)
        {
            Invoke(new Action(() => {
                _textBoxDebug.Text += $"{Environment.NewLine}{startedBy} started the game";
                _buttonStartGame.Enabled = false;
            }));
            return Task.CompletedTask;
        }

        private Task NewPlayerHandler(string arg, IEnumerable<string> players)
        {
            Invoke(new Action(() => _textBoxDebug.Text += $"{Environment.NewLine}{arg} joined"));
            return Task.CompletedTask;
        }

        private void MainFormClosing(object sender, EventArgs e)
        {
            _mqTabletopClient?.Dispose();
        }

        private void _pictureBoxMain_MouseDown(object sender, MouseEventArgs e)
        {
            var relativeClickPosition = e.Location;

            if ((ModifierKeys & Keys.Control) == Keys.Control)
            {
                _moveCompleteStack = true;
                _movingCard = GameStateRenderer.GetCardAtPosition(
                    _mqTabletopClient.GameState, relativeClickPosition,
                    _pictureBoxMain.Width, _pictureBoxMain.Height, null, out var stack, true);
                _movingStack = stack?.Cards?.ToArray() ?? new Card[] { _movingCard };
            }
            else
            {
                _moveCompleteStack = false;
                _movingCard = GameStateRenderer.GetCardAtPosition(
                    _mqTabletopClient.GameState, relativeClickPosition, 
                    _pictureBoxMain.Width, _pictureBoxMain.Height, null);
            }

            if (_movingCard == null)
            {
                return;
            }

            if (e.Button == MouseButtons.Right)
            {
                SendPutCommand(_movingCard, _movingCard.X, _movingCard.Y, _moveCompleteStack, true);
            }

            _pointerStartLocation = e.Location;
            _movingCardStartX = _movingCard.X;
            _movingCardStartY = _movingCard.Y;
        }

        private void SendPutCommand(Card card, int x, int y, bool moveStack, bool flip = false, string putOntoId = null)
        {
            if (card == null)
            {
                return;
            }

            var message = new PutCardMessage()
            {
                Card = card.Name,
                Flip = flip,
                MoveCompleteStack = moveStack,
                Id = card.Id,
                X = x,
                Y = y,
                PutOntoCardId = putOntoId,
            };

#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            _mqTabletopClient.SendMessage(message);
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
        }

        private void _pictureBoxMain_MouseMove(object sender, MouseEventArgs e)
        {
            if ((DateTime.Now - _lastSentMouseMove).TotalMilliseconds < 100)
            {
                return;
            }
            
            _lastSentMouseMove = DateTime.Now;

            if (_movingCard != null)
            {
                MoveMovingCardToPointer(e.Location);
            }
        }

        private void MoveMovingCardToPointer(Point location)
        {
            var ingameLocationDelta = GameStateRenderer.GetPositionFromLocation(_pictureBoxMain.Width,
                _pictureBoxMain.Height, location.X - _pointerStartLocation.X, location.Y - _pointerStartLocation.Y);

            SendPutCommand(_movingCard, _movingCardStartX + ingameLocationDelta.X,
                _movingCardStartY + ingameLocationDelta.Y, _moveCompleteStack);
        }

        private void _pictureBoxMain_MouseUp(object sender, MouseEventArgs e)
        {
            if (_movingCard != null)
            {
                if (!IsWithinPictureBoxMain(e.Location))
                {
                    SendTakeCommand(_movingCard, _moveCompleteStack);
                }
                else
                {
                    PutCard(e.Location);
                }

                _movingCard = null;
            }
        }

        private void PutCard(Point location)
        {
            IEnumerable<Card> exceptFor = new Card[] { _movingCard };

            if (_moveCompleteStack)
            {
                exceptFor = _movingStack;
            }

            var cardAtPosition = GameStateRenderer.GetCardAtPosition(_mqTabletopClient.GameState, location,
                _pictureBoxMain.Width, _pictureBoxMain.Height, exceptFor);

            if (cardAtPosition == null)
            {
                MoveMovingCardToPointer(location);
            }
            else
            {
                SendPutCommand(_movingCard, 0, 0, _moveCompleteStack, false, cardAtPosition.Id);
            }
        }

        private bool IsWithinPictureBoxMain(Point location)
        {
            return !(location.X < 0 || location.Y < 0 || location.X > _pictureBoxMain.Width || location.Y > _pictureBoxMain.Height);
        }

        private void SendTakeCommand(Card card, bool takeCompleteStack)
        {
            var command = new TakeCardMessage()
            {
                TakeCardId = card.Id,
                TakeStack = takeCompleteStack,
            };

#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            _mqTabletopClient.SendMessage(command);
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
        }

        private void AddShuffledDeckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var commands = GetShuffled54DeckCommands();

#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            _mqTabletopClient.SendMessages(commands);
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
        }

        private static IEnumerable<IMessage> GetShuffled54DeckCommands()
        {
            var cards = new List<string>
            {
                "cross2",
                "cross3",
                "cross4",
                "cross5",
                "cross6",
                "cross7",
                "cross8",
                "cross9",
                "cross10",
                "crossa",
                "crossj",
                "crossk",
                "crossq",
                "dia2",
                "dia3",
                "dia4",
                "dia5",
                "dia6",
                "dia7",
                "dia8",
                "dia9",
                "dia10",
                "diaa",
                "diaj",
                "diak",
                "diaq",
                "hearts2",
                "hearts3",
                "hearts4",
                "hearts5",
                "hearts6",
                "hearts7",
                "hearts8",
                "hearts9",
                "hearts10",
                "heartsa",
                "heartsj",
                "heartsk",
                "heartsq",
                "spades2",
                "spades3",
                "spades4",
                "spades5",
                "spades6",
                "spades7",
                "spades8",
                "spades9",
                "spades10",
                "spadesa",
                "spadesj",
                "spadesk",
                "spadesq",
                "joker1",
                "joker2",
            };

            return GetShuffledCardsCommands(cards);
        }

        private static List<PutCardMessage> GetShuffledCardsCommands(List<string> cards)
        {
            var shuffledCards = new List<string>();
            var random = new Random();

            while (cards.Any())
            {
                var randomIndex = random.Next(0, cards.Count - 1);
                shuffledCards.Add(cards[randomIndex]);
                cards.RemoveAt(randomIndex);
            }

            var firstCard = shuffledCards[0];

            var commands = new List<PutCardMessage>()
            {
                new PutCardMessage()
                {
                    Card = firstCard,
                    Flip = true,
                    X = 50,
                    Y = 50,
                    Id = Guid.NewGuid().ToString(),
                }
            };

            for (int i = 1; i < shuffledCards.Count; i++)
            {
                commands.Add(new PutCardMessage()
                {
                    Card = shuffledCards[i],
                    Flip = true,
                    PutOntoCardId = commands.Last().Id,
                    Id = Guid.NewGuid().ToString(),
                });
            }

            return commands;
        }

        private void _buttonStartGame_Click(object sender, EventArgs e)
        {
#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            _mqTabletopClient.StartGame();
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
        }

        private void addShuffledDeck52CardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var commands = GetShuffled52DeckCommands();

#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            _mqTabletopClient.SendMessages(commands);
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
        }

        private static IEnumerable<IMessage> GetShuffled52DeckCommands()
        {
            var cards = new List<string>
            {
                "cross2",
                "cross3",
                "cross4",
                "cross5",
                "cross6",
                "cross7",
                "cross8",
                "cross9",
                "cross10",
                "crossa",
                "crossj",
                "crossk",
                "crossq",
                "dia2",
                "dia3",
                "dia4",
                "dia5",
                "dia6",
                "dia7",
                "dia8",
                "dia9",
                "dia10",
                "diaa",
                "diaj",
                "diak",
                "diaq",
                "hearts2",
                "hearts3",
                "hearts4",
                "hearts5",
                "hearts6",
                "hearts7",
                "hearts8",
                "hearts9",
                "hearts10",
                "heartsa",
                "heartsj",
                "heartsk",
                "heartsq",
                "spades2",
                "spades3",
                "spades4",
                "spades5",
                "spades6",
                "spades7",
                "spades8",
                "spades9",
                "spades10",
                "spadesa",
                "spadesj",
                "spadesk",
                "spadesq",
            };

            return GetShuffledCardsCommands(cards);
        }

        private void _pictureBoxHandCards_MouseDown(object sender, MouseEventArgs e)
        {
            _movingHandCard = GameStateRenderer.GetHandCardAtPosition(_mqTabletopClient.GameState,
                _mqTabletopClient.IngameName, _pictureBoxHandCards.Height,
                e.Location);
        }

        private void _pictureBoxHandCards_MouseMove(object sender, MouseEventArgs e)
        {
            var mainPictureLocation = _pictureBoxMain.PointToClient(_pictureBoxHandCards.PointToScreen(e.Location));

            if (IsWithinPictureBoxMain(mainPictureLocation))
            {
                if ((DateTime.Now - _lastSentMouseMove).TotalMilliseconds < 100)
                {
                    return;
                }

                _lastSentMouseMove = DateTime.Now;
                _movingCard = _movingHandCard;
                _movingCardStartX = 0;
                _movingCardStartY = 0;
                _pointerStartLocation = new Point();
                MoveMovingCardToPointer(mainPictureLocation);
            }
            else
            {
                if (_movingHandCard == null)
                {
                    return;
                }

                _movingHandCard.X = e.Location.X;
            }
            
            Render(_mqTabletopClient.GameState);
        }

        private void _pictureBoxHandCards_MouseUp(object sender, MouseEventArgs e)
        {
            if (_movingHandCard == null)
            {
                return;
            }

            var mainPictureLocation = _pictureBoxMain.PointToClient(_pictureBoxHandCards.PointToScreen(e.Location));

            if (IsWithinPictureBoxMain(mainPictureLocation))
            {
                PutCard(mainPictureLocation);
                _movingCard = null;
            }

            _movingHandCard = null;
        }

        private void _pictureBoxMain_SizeChanged(object sender, EventArgs e)
        {
            Render(_mqTabletopClient.GameState);
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Render(_mqTabletopClient.GameState);
        }
    }
}
