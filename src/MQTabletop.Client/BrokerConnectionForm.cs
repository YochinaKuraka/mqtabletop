﻿using MQTabletop.Client.Properties;
using System.Windows.Forms;

namespace MQTabletop.Client
{
    public partial class BrokerConnectionForm : Form
    {
        public string Broker { get; private set; }
        public string User { get; private set; }
        public string Password { get; private set; }
        public string IngameUser { get; private set; }
        public bool UseTls { get; private set; }
        public string ClientId { get; private set; }
        public string Topic { get; private set; }

        public BrokerConnectionForm()
        {
            InitializeComponent();
        }

        private void Join_Click(object sender, System.EventArgs e)
        {
            Broker = _textBoxServer.Text;
            User = _textBoxUsername.Text;
            Password = _textBoxPassword.Text;
            IngameUser = _textBoxIngameName.Text;
            UseTls = _checkBoxUseTls.Checked;
            ClientId = _textBoxClientId.Text;
            Topic = _textBoxTopic.Text;

            Settings.Default.Server = _textBoxServer.Text;
            Settings.Default.Username = _textBoxUsername.Text;
            Settings.Default.UseTls = _checkBoxUseTls.Checked;
            Settings.Default.Username = _textBoxIngameName.Text;
            Settings.Default.Password = _textBoxPassword.Text;
            Settings.Default.Topic = _textBoxTopic.Text;
            Settings.Default.Save();

            DialogResult = DialogResult.OK;
            Close();
        }

        private void BrokerConnectionForm_Load(object sender, System.EventArgs e)
        {
            _textBoxServer.Text = Settings.Default.Server;
            _textBoxUsername.Text = Settings.Default.Username;
            _checkBoxUseTls.Checked = Settings.Default.UseTls;
            _textBoxIngameName.Text = Settings.Default.Username;
            _textBoxPassword.Text = Settings.Default.Password;
            _textBoxTopic.Text = Settings.Default.Topic;
        }
    }
}
